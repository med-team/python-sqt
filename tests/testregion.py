from sqt.region import Region

def test_region():
	regions = [
		("chr7:5-7", ("chr7", 4, 7, False)),
		("chr7", ("chr7", 0, None, False)),
		("rc:chr7", ("chr7", 0, None, True)),
		("chr7:1-100", ("chr7", 0, 100, False)),
		("rc:chr7:1-100", ("chr7", 0, 100, True)),
		("chr7:20-1,000,000", ("chr7", 19, 1000000, False)),
		("rc:chr7:1-999,999", ("chr7", 0, 999999, True)),
		("chr7:5-", ("chr7", 4, None, False)),
		("rc:chr7:5-", ("chr7", 4, None, True)),
		("chr7:20", ("chr7", 19, 20, False)),
		("rc:chr7:1,200..999,999", ("chr7", 1199, 999999, True)),
		("chr7:12..", ("chr7", 11, None, False)),
		#("chr7:1-", ("chr7", 0, None, False)), # will not round-trip
	]
	for spec, (reference, start, stop, is_reverse_complement) in regions:
		region = Region(spec)
		assert region.reference == reference
		assert region.start == start
		assert region.stop == stop
		assert region.is_reverse_complement == is_reverse_complement
		assert "{}".format(region) == spec.replace(',', '').replace('..', '-'), (region, spec)

		r1 = Region(reference, start, stop, is_reverse_complement)
		r2 = Region(spec)
		assert r1 == r2, (r1, r2)

